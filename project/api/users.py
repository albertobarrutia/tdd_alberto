# project/api/users.py
from flask import Blueprint, request
from flask_restplus import Api, Resource, fields

from project import db
from project.api.models import User
from project.api.services import (add_user, delete_user, get_all_users,
                                  get_user_by_email, get_user_by_id,
                                  update_user)

# from sqlalchemy import exc


users_blueprint = Blueprint("users", __name__)
api = Api(users_blueprint)

user = api.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


# project/api/users.py
# new


class UsersList(Resource):
    # @api.expect(user, validate=True) # new
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_email(email)
        if user:
            response_object["message"] = "Sorry. That email already exists"
            return response_object, 400

        # Con el validador si lo sdatso no cumplen con el modelo no llega aqui
        try:
            # db.session.add(User(username=username, email=email))
            # db.session.commit()
            add_user(username, email)
            response_object["message"] = f"{email} was added!"
            return response_object, 201
        except Exception:
            db.session.rollback()
            response_object["message"] = f"Input payload validation failed"
            return response_object, 400

    @api.marshal_with(user, as_list=True)
    def get(self):
        return get_all_users(), 200  # User.query.all(), 200


class UserList(Resource):
    @api.expect(user, validate=True)
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = User.query.filter_by(email=email).first()
        if user:
            response_object["message"] = "Sorry. That email alreadey exists"
            return response_object, 400
        try:
            db.session.add(User(username=username, email=email))
            db.session.commit()
            response_object["message"] = f"{email} was added!"
            return response_object, 201
        except Exception:
            db.session.rollback()
            response_object["message"] = f"Input payload validation failed"
            return response_object, 400

    @api.marshal_with(user, as_list=True)
    def get(self):
        return User.query.all(), 200


class Users(Resource):
    @api.marshal_with(
        user
    )  # este decorador se utiliza para pasar los datso del get a un JSON
    def get(self, user_id):
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        return user, 200

    def delete(self, user_id):
        response_object = {}
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        # db.session.delete(user)
        # db.session.commit()
        delete_user(user)
        response_object["message"] = f"{user.email} was removed!"
        return response_object, 200

    @api.expect(user, validate=True)
    def put(self, user_id):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        # user.username = username
        # user.email = email
        # db.session.commit()
        update_user(user, username, email)
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


api.add_resource(Users, "/users/<int:user_id>")
# api.add_resource(UsersList, '/users')
api.add_resource(UserList, "/users")
