from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import os

# from flask_restplus import Api,Resource

db = SQLAlchemy()


def create_app(script_info=None):
    app = Flask(__name__)

    app_settings = os.getenv("APP_SETTINGS")  # incluye direccion fichero
    app.config.from_object(app_settings)
    from project.api.users import users_blueprint

    app.register_blueprint(users_blueprint)

    db.init_app(app)

    from project.api.ping import ping_blueprint

    app.register_blueprint(ping_blueprint)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app


"""


# instantiate the app
app = Flask(__name__)

app.config.from_object('project.config.DevelopmentConfig') # new
app_settings = os.getenv('APP_SETTINGS')
app.config.from_object(app_settings)

import sys
print (app.config , file=sys.stderr)


api = Api(app)


class Ping(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }


api.add_resource(Ping, '/ping')

"""
