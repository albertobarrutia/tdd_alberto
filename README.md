 ![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)
 ![Python](https://img.shields.io/badge/python-3.7-blue)

[![pipeline status](https://gitlab.com/albertobarrutia/tdd_alberto/badges/master/pipeline.svg)]


# P2 – Container-based Continuous applicaton orchestration and Deployment

Este proyecto contiene el trabajo realizado durante la realización de la P2 de la asignatura 'Integración y despliege continuo'. En ella, se ha cogido como base una aplicación desarrollada anteriormente. Esta aplicación estaba compuesta por una base de datos y un servicio de API que además de responder a las peticiones, era utilizado para modificar la BBDD.


## Puesta en marcha

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

* Clonar el repositorio en tu máquina, con algún comando como 'git clone https://gitlab.com/albertobarrutia/tdd_alberto.git'
* Entrar en la carpeta: cd tdd_alberto
* Lanzar los contenedores: sudo docker-compose up --build -d


## Changelog
Con el desarrollo de esta práctica se ha liberado una nueva release del proyecto en cuestión. EStos son los principales cambios realizados:

- Despliegue de la aplicación en local utilizando Minikube
- Despliegue de la aplicación utilizando la plataforma GCloud
- Creación de un pipeline CI/CD, donde, cogiendo como base el anterior pipeline CI (build & test), se extiende este para automatizar el también despliegue en GCloud.
- Se ha gestionado el desarrollo en base a la metodología ágil descrita en doc.md

## Documentación
Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos
en la documentación complementaria doc.md

## Quick start

* Clonar el repositorio desde gitlab:
```bash
git clone https://gitlab.com/albertobarrutia/tdd_alberto.git
cd tdd_alberto
```
 

### Lanzar la aplicación utilizando docker-compose:

```bash
docker-compose up -d --build
```

## Authors

Alberto Barrutia - @albertobarrutia

## License
This project is licensed under the MIT License. See LINCENSE.md for details

