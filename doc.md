# P2 – Container-based Continuous applicaton orchestration and Deployment
---


## Preparación del entorno DEV en minikube

En este apartado se ha desplegado la aplicación minikube en local. Todas las plantillas yml utilizadas para este despliegue se encuentran en la carpeta k8s.

Comandos a ejecutar para poner en marcha el despliegue:

```bash
kubectl create configmap postgres --from-file=project/db/create.sql
kubectl create -f k8s/postgres-configmap.yml
kubectl create -f k8s/secrets.yml
kubectl create -f k8s/postgres-deployment.yml
kubectl create -f k8s/postgres-service.yml
kubectl create -f k8s/users-deployment.yml
kubectl create -f k8s/users-service-nodeport.yml

```

Listado de los pods creados:

![Screenshot](img/getpods.png)

Listado de los servicios creados:

![Screenshot](img/getpods.png)

Comprobación del funcionamiento del despliegue minikube:

![Screenshot](img/pinguserslocal.png)


## Desplegar en GCloud la aplicación desde consola

Aquí utilizamos el CLI de gcloud para hacer una inicialización (autenticación, creación de un clúster, etc.) y después desplegar la aplicación allí.

Estos son los comandos utilizados:

```bash

gcloud iam service-accounts create staging \
--display-name "My staging Service Account"

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
--member serviceAccount:staging@${PROJECT_ID}.iam.gserviceaccount.com \
--role roles/owner
gcloud iam service-accounts keys create ~/key.json --iam-account \
staging@${PROJECT_ID}.iam.gserviceaccount.com
export GOOGLE_APPLICATION_CREDENTIALS="/home/${USER}/key.json"
cp /home/${USER}/key.json ./key.json

gcloud auth login 
gcloud config set project name
gcloud auth activate-service-account --key-file ./key.json
gcloud config set account alberto_barrutia
gcloud config set project alberto-barrutia
gcloud config set container/use_client_certificate True
gcloud config unset container/use_client_certificate
gcloud config set compute/region europe-west1
gcloud container clusters create mycluster --region us-central1-a
gcloud container clusters resize mycluster --num-nodes 1 --quiet --region us-central1-a
sudo ntpdate -s time.nist.gov

```

Detalles del clúster creado:

![Screenshot](img/clusterinfo.png)

Comprobación del funcionamiento de la aplicación desplegada en GCloud:

![Screenshot](img/googletest.png)

## Automatizar el despliegue mediante un pipeline CI/CD

En el fichero .gitlab-ci.yml se ha definido un pipeline donde se automatizan los procesos de build, test, delivery y deploy en la plataforma gitlab. Como principal variante a lo hecho anteriormente, decir que esta vez hemos añadido un despliegue canary, que será administrado y gestionado por el balanceador de carga. 

Las etapas del pipeline son las siguientes:

- build
- test
- delivery canary
- canary
- delivery
- deployment

Y el estado del pieline se puede consultar aquí:

[![pipeline status](https://gitlab.com/albertobarrutia/tdd_alberto/badges/master/pipeline.svg)]

## Se ha gestionado el desarrollo en base a la metodología ágil

Todo el proceso se ha basado en la metodología Agile. Prueba de ello son los siguientes apartados:

### issues
Se han utilizado los issues cómo unidad mínima de trabajo. Se ha intentado hacer uso de ellos para diferenciar de la mejor manera posible todos los procesos o tasks necesarios para avanzar en el proyecto.

### labels
Han sido asignadas a los diferentes issues según el tipo de estos. Se han diferenciado dos tipos de labels: por un lado, los relativos al ámbito en el que se encuentra esa tarea (testeo de la aplicación, automatización, documentación, etc.) y por otro lado los que dependen de la fecha tiempo de esa tarea (Not started, next_deliverable...).

### board 
Se ha utilizado un board para distinguir las diferentes tareas según su fecha límite o fecha estimada. Además de las dos tablas open y close que se incluyen por defecto, se han añadido los anteriormente mencionados labels temporales para concretar un poco más el estado de estos issues seg´un sus plazos. Así pues, las cuatro tablas en las que clasificaremos los issues son: open, not started, next deliverable y close.


### milestones
Están compuestos por diferentes issues. Cada uno de ellos identifica un hito que se debe cumplir en el proyecto, además del espacio de tiempo en el que este debería ser completado.

### commits
Las actualizaciones progresivas del código han sido integradas mediante diferentes commits, cuyos nombres identifican el cambio que se ha producido respecto a la versión anterior. Al haber sido un trabajo realizado por un solo usuario, no se han dado situaciones de conflictos de versiones o de división de ramas.
